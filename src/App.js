import React, {useState} from 'react';
import {Header} from './component/Header'
import {Footer} from './component/Footer'
import './component/stylesheet.css'

const App = () => {

  const [header, setHeader] = useState('Foo')   // Hook
  
  return (
    <div className="App AppColor">
      <div><Header name={header}/></div>
      <button onClick={()=> setHeader('Bar')}>Change Header</button>
      <div>HELLO WORLD</div>
      <div><Footer /></div>
    </div>)
}
export default App
